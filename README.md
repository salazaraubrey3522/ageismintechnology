# Ageism in technology

Ageism is a challenge in the technology industry.

This page documents common resources for those who wish to educate themselves and take action on this topic.

# Want to get involved?

1. Read this page
1. Join the [public discussion on the AgeismInTechnology on Slack](https://join.slack.com/t/ageismintechnology/shared_invite/zt-l40w2clh-tdJ2_iE4dmP0oTc6OMSWug)
1. Create a merge request to update this page



# Wisdom at Work

[Wisdom at Work](https://www.amazon.com/dp/B078LS21ZX/) originally inspired the ideas fo this group.  It spawned the ideas for a "team member resource group" at [GitLab](https://about.gitlab.com) which spawned the idea for reaching out via a [PlatoHQ Circle](https://www.platohq.com/circles?page=1&query=&&) to those outside the company.

> Part manifesto and part playbook, Wisdom@Work ignites an urgent conversation about ageism in the workplace, calling on us to treat age as we would other type of diversity. In the process, Conley liberates the term "elder" from the stigma of "elderly," and inspires us to embrace wisdom as a path to growing whole, not old. Whether you've been forced to make a mid-career change, are choosing to work past retirement age, or are struggling to keep up with the millennials rising up the ranks, Wisdom@Work will help you write your next chapter.

## Common misperceptions about older workers:

* Poor performers and less engaged
* Resistant to change
* Less able to learn
* Shorter tenure
* More costly
* Less trusting
* Less healthy
* Work-family imbalance
* Technical skills not up-to-date

# Articles and interesting links

* [GitLab: Generational Understanding TMRG (team member resource group)](https://about.gitlab.com/company/culture/inclusion/tmrg-gitlab-generational-understanding/)
* [GitLab: Offer to speaking with TMRG members in the hiring process](/company/culture/inclusion/recruiting-initiatives/#speaking-with-tmrg-members-in-the-hiring-process)
* [GitLab: Set expectations on dates on resumes](/company/culture/inclusion/recruiting-initiatives/#lack-of-dates-on-your-resume)
* [Arxiv: Is 40 the new 60? How popular media portrays the employability of older software developers](https://arxiv.org/ftp/arxiv/papers/2004/2004.05847.pdf)
* [Wired: Surviving as an Old in the Tech World](https://www.wired.com/story/surviving-as-an-old-in-the-tech-world/)
* [LinkedIn: Google Leaders & "Greyglers"​ On Navigating Our Way to Bigger and Better Things](https://www.linkedin.com/pulse/google-leaders-greyglers-navigating-our-way-bigger-better-tracy-wilk/)
* [U.S. Equal Employment Opportunity Commission: Age Discrimination](https://www.eeoc.gov/age-discrimination)
* [Psychology Today:"Young People Are Just Smarter"](https://www.psychologytoday.com/us/blog/boomers-30/201710/young-people-are-just-smarter)
* [LinkedIn:Thriving across our differences](https://www.linkedin.com/learning/confronting-bias-thriving-across-our-differences/outro-with-arianna-huffington)
* [Better Allies: Everyday Actions to Create Inclusive, Engaging Workplaces](https://www.amazon.com/gp/product/1732723311/)
* [Age Implicit Association Test](https://implicit.harvard.edu/implicit/takeatest.html) - a great resource for testing for concious and unconcious bias (on age and other factors)



